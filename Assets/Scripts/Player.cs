﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField]
    private float _speed = 3.5f;
    [SerializeField]
    private GameObject _laserPrefab;
    [SerializeField]
    private GameObject _TripleShotPrefab;
    [SerializeField]
    private float _fireRate = 0.5f;
    private float _canFire = -1f;
    [SerializeField]
    private int _lives = 3;
    private SpawnManager _spawnManager;
    private bool _isTripleShotActive = false; 
    [SerializeField]
    private float _powerUpCoolDown = 5.0f; 
    private bool _isSpeedBoostActive = false; 
    [SerializeField]
    private int _SpeedBostMultiplier = 2;
    private bool _isShieldActive = false; 
    [SerializeField]
    private GameObject _shieldFx;
    [SerializeField]
    private int _score;
    private UIManager _uIManager;
    [SerializeField]
    private GameObject[] _engines;
    private AudioSource _audioSource;
    [SerializeField]
    private AudioClip _sfxLaser;

    [SerializeField]
    private AudioClip _sfxPowerUp;
// Add audio clip


    void Start()
    {
        transform.position = new Vector3(0,0,0);

        _spawnManager = GameObject.Find("Spawn_Manager").GetComponent<SpawnManager>();

        if (_spawnManager == null)
        {
            Debug.LogError("The spawn manager is null");
        }

        _uIManager = GameObject.Find("Canvas").GetComponent<UIManager>();
        if (_uIManager == null)
        {
            Debug.LogError("The UI manager is null");
        }

        _audioSource = GetComponent<AudioSource>();
        if (_audioSource == null)
        {
            Debug.LogError("The Player Audio Source is null");
        }
    }


    void Update()
    {
        CalculateMovement();
        
        if(Input.GetKeyDown(KeyCode.Space) && Time.time > _canFire){
            ShootLaser();
        }
    }


    void CalculateMovement(){
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        transform.Translate(Time.deltaTime*_speed*horizontalInput,Time.deltaTime*_speed*verticalInput,0);

        transform.position = new Vector3(transform.position.x,Mathf.Clamp(transform.position.y,-3.8f,0),transform.position.z);

        if(transform.position.x >= 11){
            transform.position = new Vector3(-11,transform.position.y,transform.position.z);
        }
        else if (transform.position.x <= -11){
             transform.position = new Vector3(11,transform.position.y,transform.position.z);
        }
    }


    void ShootLaser(){
        _canFire = Time.time + _fireRate;

        if (_isTripleShotActive)
        {
            Instantiate(_TripleShotPrefab,transform.position, Quaternion.identity);
        }
        else
        {
            Instantiate(_laserPrefab,new Vector3(transform.position.x,transform.position.y + 1f,transform.position.z), Quaternion.identity);
        }

        _audioSource.clip = _sfxLaser;
        _audioSource.Play();
        // play laser audio clip
    }


    public void Damage(){
        if (_isShieldActive)
        {
            _isShieldActive = false;
            _shieldFx.SetActive(false);
            return;
        }

        _lives -= 1;

        if(_lives == 2)
        {
            _engines[Random.Range(0, 2)].SetActive(true);
        }
        
        if (_lives == 1)
        {
            _engines[0].SetActive(true);
            _engines[1].SetActive(true);
        }

        _uIManager.UpdateLives(_lives);
        
        if(_lives < 1){
            _spawnManager.OnPlayerDeath();
            Destroy(gameObject);
        }
    }


    public void TripleShotActive()
    {
        _isTripleShotActive = true;
        _audioSource.clip = _sfxPowerUp;
        _audioSource.Play();
        StartCoroutine(TripleShotPowerDownRoutine());
    }


    IEnumerator TripleShotPowerDownRoutine(){
        yield return new WaitForSeconds(_powerUpCoolDown);
        _isTripleShotActive = false;
    }


    public void SpeedBoostActive()
    {
        _isSpeedBoostActive = true;
        _speed = _speed *_SpeedBostMultiplier;
        _audioSource.clip = _sfxPowerUp;
        _audioSource.Play();
        StartCoroutine(SpeedBoostPowerDownRoutine());
    }


    IEnumerator SpeedBoostPowerDownRoutine(){
        yield return new WaitForSeconds(_powerUpCoolDown);
        _isSpeedBoostActive = false;
        _speed = _speed /_SpeedBostMultiplier;
    }


    public void ShieldActive()
    {
        _isShieldActive = true;
        _shieldFx.SetActive(true);
        _audioSource.clip = _sfxPowerUp;
        _audioSource.Play();
        StartCoroutine(TripleShotPowerDownRoutine());
    }


    public void AddScore(int points)
    {
        _score += points;
        _uIManager.ShowScore(_score);
    }
    // Add value to score
    // Comnicate with ui to update score
}
