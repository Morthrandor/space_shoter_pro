﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour
{
    [SerializeField]
    private float _speed = 3.0f;
    [SerializeField]
    private GameObject _explosionPrefab;
    private SpawnManager _spawnmManager;
    

    private void Start() 
    {
        _spawnmManager = GameObject.Find("Spawn_Manager").GetComponent<SpawnManager>();
    }


    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(0,0,_speed*Time.deltaTime));
        // rotate object on z axys 3 mts/s
    }


    // check for lazer collisioin
    // instantiate explosion at position of asteroid (us)
    // destroy explosion after 3 secons
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Laser")
        {
            Instantiate(_explosionPrefab, transform.position, Quaternion.identity);
            Destroy(other.gameObject); 
            _spawnmManager.StartSpawning();
            Destroy(this.gameObject, 0.25f);
        }
    }
}
