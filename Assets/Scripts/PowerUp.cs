﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    [SerializeField]
    private float _speed = 3.0f;
    [SerializeField]
    private int _idPowerUp;
    // ids powerups
    // 0,triple, 1 speed 2 shield


    void Update()
    {
        transform.Translate(Vector3.down*Time.deltaTime*_speed);
        if(transform.position.y < -6.5f){
            Destroy(gameObject); 
        }
    }


    private void OnTriggerEnter2D(Collider2D other) {
        if(other.tag == "Player"){
            Player player = other.transform.GetComponent<Player>();
            if(player != null)
            {
                switch (_idPowerUp)
                {
                    case 0:
                        player.TripleShotActive();
                        break;
                    case 1:
                        player.SpeedBoostActive();
                        break;
                    case 2:
                        player.ShieldActive();
                        break;
                    default:
                        Debug.Log("Default Case");
                        break;
                }
            }
            Destroy(gameObject);
        }
    }
}
