﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    // create handle to text
    [SerializeField]
    private Text _scoreText;
    [SerializeField]
    private Image _LivesImg;
    [SerializeField]
    private Sprite[] _liveSprites;
    [SerializeField]
    private Text _GameOverText;
    [SerializeField]
    private Text _RestartText;
    private GameManager _gameManager;


    // Start is called before the first frame update
    void Start()
    {
        ShowScore(0);
        _GameOverText.gameObject.SetActive(false);
        _RestartText.gameObject.SetActive(false);

        _gameManager = GameObject.Find("Game_Manager").GetComponent<GameManager>();
        if (_gameManager == null)
        {
            Debug.LogError("The Game manager is null");
        }
    }


    public void ShowScore(int score)
    {
        _scoreText.text = "Score: " + score;
    }


    public void UpdateLives(int currentLives){
        _LivesImg.sprite = _liveSprites[currentLives];
        if (currentLives == 0)
        {           
            GameOverSecuence();
        }
    }


    private void GameOverSecuence()
    {
        StartCoroutine(GamerOverFlickerRoutine());
        _RestartText.gameObject.SetActive(true);
        _gameManager.GameOver();
    }


    IEnumerator GamerOverFlickerRoutine()
    {
        while (true)
        {
            _GameOverText.gameObject.SetActive(true);
             yield return new WaitForSeconds(0.5f);
            _GameOverText.gameObject.SetActive(false);
             yield return new WaitForSeconds(0.5f);
        }
    }
}
