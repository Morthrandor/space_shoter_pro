﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField]
    private float _speed = 4.0f;

    private Player _player;
    private Animator _animator;
    private AudioSource _audioSource;
    [SerializeField]
    private GameObject _laserPrefab;
    private float _fireRate = 3.0f;
    private float _canFire = -1;


// Handle anim component
    private void Start() 
    {
        // null check player
        _player = GameObject.Find("Player").GetComponent<Player>();
        if (_player == null)
        {
            Debug.LogError("The player is null");
        }
        _animator = GetComponent<Animator>();
        if (_animator == null)
        {
            Debug.LogError("The animator is null");
        }
        _audioSource = GetComponent<AudioSource>();
        if (_audioSource == null)
        {
            Debug.LogError("The  audio source is null");
        }
    }


    void Update()
    {
        CalculateMovement();
        
        if (Time.time > _canFire)
        {
            _fireRate = UnityEngine.Random.Range(3f,7f);
            _canFire = Time.time + _fireRate;
            GameObject enemyLaser = Instantiate(_laserPrefab,transform.position, Quaternion.identity);

            Laser[] lasers = enemyLaser.GetComponentsInChildren<Laser>();
            foreach(Laser laser in lasers)
            {
                laser.AssignEnemyLaser();
            }
        }
    }


    void CalculateMovement()
    {
        transform.Translate(Vector3.down*Time.deltaTime*_speed);

        if(transform.position.y < -6.5f){
            var randomX = UnityEngine.Random.Range(-8f, 8f);
            transform.position = new Vector3(randomX,8f,transform.position.z);
        }
    }


    private void OnTriggerEnter2D(Collider2D other) {
        if(other.tag == "Player")
        {
            if(_player != null)
            {
                _player.Damage();
                _player.AddScore(10);
            }
            _animator.SetTrigger("OnEnemyDeath");
            _speed = 0;
            _audioSource.Play();
            Destroy(GetComponent<Collider2D>()); 
            Destroy(gameObject,2.8f);

        }

        if(other.tag == "Laser")
        {
            Destroy(other.gameObject); 

            if(_player != null)
            {
                _player.AddScore(10);
            }
            _animator.SetTrigger("OnEnemyDeath");
            _speed = 0;
            _audioSource.Play();
            Destroy(GetComponent<Collider2D>());  
            Destroy(gameObject,2.8f); 
        }
    }
}
